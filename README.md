# Volcanic dispersal at local scale: a reanalysis of the 2021 La Palma case

**SC leader:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/csic.png?inline=false" width="80">

**Partners:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/INGV.png?inline=false" width="35">

**Codes:** [FALL3D](https://gitlab.com/cheese5311126/codes/fall3d)

**Target EuroHPC Architectures:** MN5

**Type:** EWS, urgent computing

## Description

Recent volcanic eruptions in Europe (La Palma in the Canaries and Fagradalsfjall
in Iceland) raised major concerns for public health as well as for environmental
impact and brought attention to volcanic events characterised by sustained
low-intensity gas (mainly SO2) and ash emissions affecting the lower
atmosphere during several weeks. These events took place in small
islands/peninsulas with sensitive targets (e.g., towns, villages, infrastructures or
airports) highly exposed to the ash and gases released during the eruptions. In
particular, the eruption at the Cumbre Vieja volcanic ridge, on the island of La
Palma in the Canary Islands (Spain), started on 19 September 2021, was the
most damaging volcanic eruption ever recorded on La Palma and resulted in
huge economic losses. The ability to simulate such events occurring over very
small spatial scales and produce reliable forecasts has shown to be challenging
due to the presence of very complex terrain and the strong interaction between
the atmosphere and the eruption dynamics leading to micrometeorology induced
by the lava field, conversion of SO2 in sulphates when the gas particles are
interacting with the water droplets and the effect of absorption of incoming
shortwave radiation by aerosols on the volcanic plume dynamics. The SC5.2
aims to simulate the volcanic gas dispersal at local scale and generate a
reanalysis at 1-2 km resolutions of the 2021 eruption of Cumbre Vieja, an active
volcanic ridge on the island of La Palma in the Canary Islands, Spain. To this
purpose, we will make use of numerical models, HPC resources and observations
available from different sources in order to develop a downscaling strategy
capable of being efficiently applied in future small-scale events.

<img src="SC5.2.png" width="800">

**Figure 3.8.1:** Graphical abstract of SC5.2 showing the domain configuration with three
nested domains to be used for downscaling the meteorological fields. The domains are
centred around Cumbre Vieja, an active volcanic ridge on the island of La Palma in the
Canary Islands, Spain. The latest eruption started on 19 September 2021 will be
simulated using the high resolution meteorological fields provided by the downscaling
strategy described in this document.

## Expected results

This SC will do a reanalysis of the September-December 2021 La Palma event at 1-2 km resolution
making use of different observations available for tuning and verifying the model performance for a more reliable and robust
forecast. This will showcase a potential UC service for volcanic dispersal forecasts at local scale.